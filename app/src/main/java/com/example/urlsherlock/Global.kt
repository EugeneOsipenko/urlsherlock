package com.example.urlsherlock

import android.util.Log

const val URL_PREFIX = "https://"

fun log(m: String) = Log.e("Sherlock", m)
