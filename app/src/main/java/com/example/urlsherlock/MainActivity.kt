package com.example.urlsherlock

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.urlsherlock.model.SearchOptions
import com.example.urlsherlock.model.SearchResultItem
import com.example.urlsherlock.options.OptionsDialog
import com.example.urlsherlock.options.OptionsSelectedListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

@SuppressLint("CheckResult")
class MainActivity : AppCompatActivity(), OptionsSelectedListener {

    private val engine = SearchEngine()
    private var options: SearchOptions? = null

    private val searchItems = mutableListOf<SearchResultItem>()
    private val adapter = SearchAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        findViewById<Button>(R.id.start).setOnClickListener { startSearch() }
        findViewById<Button>(R.id.stop).setOnClickListener { engine.stop() }
        findViewById<Button>(R.id.options).setOnClickListener { options() }
        findViewById<RecyclerView>(R.id.items).adapter = adapter

        val status = findViewById<TextView>(R.id.status)

        engine.textStatus
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { status.text = it }

        engine.searchStatus
            .map {
                if (searchItems.contains(it)) {
                    searchItems[searchItems.indexOf(it)] = it
                } else {
                    searchItems.add(it)
                }
            }
            .throttleWithTimeout(200, TimeUnit.MILLISECONDS)
            .map { mutableListOf<SearchResultItem>().apply { addAll(searchItems) } }
            .map { DiffUtil.calculateDiff(SearchDiffCallback(adapter.getData(), it)) to it }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { pair ->
                adapter.setData(pair.second)
                pair.first.dispatchUpdatesTo(adapter)
            }
    }

    override fun onOptionsSelected(options: SearchOptions) {
        this.options = options
        findViewById<Button>(R.id.start).isEnabled = true
    }

    private fun options() {
        val dialog = OptionsDialog()
        dialog.show(supportFragmentManager, "options")
    }

    private fun startSearch() {
        options?.let {
            searchItems.clear()
            engine.search(it)
        }
    }
}
