package com.example.urlsherlock

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.urlsherlock.model.SearchResultItem
import com.example.urlsherlock.model.SearchStatus

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    private val items = mutableListOf<SearchResultItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        return SearchViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        items[position].apply {
            holder.url.text = url
            holder.status.text = status.toString()
            holder.statusColor.setBackgroundColor(
                when (status) {
                    SearchStatus.LOADING -> Color.YELLOW
                    SearchStatus.NOT_FOUND -> Color.GRAY
                    SearchStatus.ERROR -> Color.RED
                    SearchStatus.FOUND -> Color.GREEN
                }
            )

            if (error.isNotEmpty()) {
                holder.status.text = error
            }
        }
    }

    fun setData(items: List<SearchResultItem>) {
        this.items.clear()
        this.items.addAll(items)
    }

    fun getData() = items

    inner class SearchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val url: TextView = view.findViewById(R.id.url)
        val status: TextView = view.findViewById(R.id.status)
        val statusColor: View = view.findViewById(R.id.status_color)
    }
}