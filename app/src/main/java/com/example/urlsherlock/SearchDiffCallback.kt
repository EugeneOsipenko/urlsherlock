package com.example.urlsherlock

import androidx.recyclerview.widget.DiffUtil
import com.example.urlsherlock.model.SearchResultItem

class SearchDiffCallback(
    private val oldList: List<SearchResultItem>,
    private val newList: List<SearchResultItem>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition].url == newList[newItemPosition].url

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem.url == newItem.url
                && oldItem.status == newItem.status
                && oldItem.error == newItem.error
    }
}