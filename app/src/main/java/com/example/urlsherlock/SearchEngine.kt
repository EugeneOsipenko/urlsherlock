package com.example.urlsherlock

import android.annotation.SuppressLint
import com.example.urlsherlock.model.SearchOptions
import com.example.urlsherlock.model.SearchResultItem
import com.example.urlsherlock.model.SearchStatus
import com.example.urlsherlock.model.UrlNode
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicBoolean


@SuppressLint("CheckResult")
class SearchEngine {

    val textStatus = PublishSubject.create<String>()
    val searchStatus = PublishSubject.create<SearchResultItem>()

    private val urlLoader = UrlLoader()
    private val queue: Queue<List<String>> = LinkedList()
    private val visited = mutableSetOf<String>()
    private val searching = AtomicBoolean()

    private lateinit var scheduler: Scheduler
    private lateinit var options: SearchOptions

    private var stopper = BehaviorSubject.create<Boolean>()
    private var searchedPageCount = 0

    init {
        RxJavaPlugins.setErrorHandler { error -> error.printStackTrace() }
    }

    fun search(options: SearchOptions) {
        this.options = options
        this.stopper = BehaviorSubject.create()
        this.scheduler = Schedulers.from(Executors.newFixedThreadPool(options.maxThreadCount))
        this.queue.clear()
        this.visited.clear()
        this.searching.set(true)
        this.searchedPageCount = 0

        textStatus.onNext("Searching started")
        depthSearch(listOf(options.url))
    }

    fun stop() = stopSearch("Stopped by the user")

    private fun depthSearch(urls: List<String>) {
        Observable.fromIterable(urls)
            .flatMap { loadUrl(it) }
            .takeUntil(stopper)
            .observeOn(Schedulers.single())
            .subscribe(
                { node -> searchHtml(node) },
                { error -> error.printStackTrace() },
                { next() })
    }

    private fun loadUrl(url: String) =
        urlLoader
            .load(url)
            .doOnSubscribe { searchStatus.onNext(SearchResultItem(url, SearchStatus.LOADING)) }
            .subscribeOn(scheduler)
            .takeUntil(stopper)

    private fun searchHtml(node: UrlNode) {
        if (!searching.get() || visited.contains(node.url)) return
        if (searchedPageCount++ >= options.maxPageCount) {
            stopSearch("Result not found: reached page limit")
            return
        }

        visited.add(node.url)

        if (node.error.isNotEmpty()) {
            searchStatus.onNext(SearchResultItem(node.url, SearchStatus.ERROR, node.error))
            return
        }

        log("Searching on page ${node.url} + $searchedPageCount")

        val found = isResultInDoc(node, options.text)
        if (found) {
            searchStatus.onNext(SearchResultItem(node.url, SearchStatus.FOUND))
            stopSearch("Found result on page ${node.url}")
            return
        } else {
            searchStatus.onNext(SearchResultItem(node.url, SearchStatus.NOT_FOUND))
            if (node.links.isNotEmpty()) {
                queue.add(node.links.filter { !visited.contains(it) })
            }
        }
    }

    private fun next() {
        if (!searching.get()) return

        val next = queue.poll()
        if (next == null) {
            stopSearch("Result not found: no more url's to search")
        } else {
            depthSearch(next)
        }
    }

    private fun stopSearch(reason: String) {
        searching.set(false)
        textStatus.onNext(reason)
        stopper.onNext(true)
    }

    private fun isResultInDoc(node: UrlNode, target: String) = node.text.contains(target)
}