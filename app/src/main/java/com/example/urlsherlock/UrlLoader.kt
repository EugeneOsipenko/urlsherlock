package com.example.urlsherlock

import com.example.urlsherlock.model.UrlNode
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jsoup.Jsoup
import java.io.InterruptedIOException

class UrlLoader {

    private val client = OkHttpClient.Builder().build()

    fun load(url: String) =
        Observable.create<UrlNode> { emitter ->
            if (emitter.isDisposed) return@create

            try {
                val response = client.newCall(Request.Builder().url(url).build()).execute()

                if (!emitter.isDisposed) {
                    val body = response.body
                    emitter.onNext(
                        if (response.isSuccessful && body != null) {
                            toUrlNode(url, body.string())
                        } else {
                            UrlNode(url = url, error = "Error - reason: ${response.message} code: ${response.code}")
                        }
                    )

                    emitter.onComplete()
                }
            } catch (exception: InterruptedIOException) {
                if (!emitter.isDisposed) {
                    emitter.onComplete()
                }

                return@create
            }
        }

    private fun toUrlNode(url: String, html: String): UrlNode {
        val doc = Jsoup.parse(html)
        val text = doc.wholeText()
        val links = doc.select("a[href]")
            .filter { it.attr("href").startsWith(URL_PREFIX) }
            .map { element -> element.attr("href") }

        return UrlNode(url, text, links)
    }
}