package com.example.urlsherlock.model

data class SearchOptions(
    val url: String,
    val maxThreadCount: Int,
    val text: String,
    val maxPageCount: Int
)