package com.example.urlsherlock.model

data class SearchResultItem(
    val url: String,
    val status: SearchStatus,
    val error: String = ""
) {

    override fun equals(other: Any?) = other is SearchResultItem && url == other.url

    override fun hashCode() = url.hashCode()
}