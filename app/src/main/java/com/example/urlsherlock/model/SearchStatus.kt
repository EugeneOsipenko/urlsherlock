package com.example.urlsherlock.model

enum class SearchStatus {
    LOADING,
    NOT_FOUND,
    FOUND,
    ERROR
}