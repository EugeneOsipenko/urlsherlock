package com.example.urlsherlock.model

data class UrlNode(
    val url: String = "",
    val text: String = "",
    val links: List<String> = arrayListOf(),
    val error: String = ""
)