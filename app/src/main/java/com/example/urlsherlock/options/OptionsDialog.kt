package com.example.urlsherlock.options

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.urlsherlock.R
import com.example.urlsherlock.model.SearchOptions

class OptionsDialog : DialogFragment() {

    private lateinit var listener: OptionsSelectedListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_options, null)

        val dialog = AlertDialog.Builder(requireContext())
            .setTitle("Search options")
            .setView(view)
            .setPositiveButton(android.R.string.ok) { dialog, which -> buildOptions(view) }
            .setNegativeButton(android.R.string.cancel) { dialog, which -> dialog.dismiss() }
            .create()

        return dialog
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (requireActivity() !is OptionsSelectedListener) {
            throw IllegalStateException("Activity must inherit from OptionsSelectedListener to receive result")
        }

        listener = requireActivity() as OptionsSelectedListener
    }

    private fun buildOptions(view: View) {
        listener.onOptionsSelected(
            SearchOptions(
                view.findViewById<EditText>(R.id.url).text.toString(),
                view.findViewById<EditText>(R.id.thread_count).text.toString().toInt(),
                view.findViewById<EditText>(R.id.text).text.toString(),
                view.findViewById<EditText>(R.id.page_count).text.toString().toInt()
            )
        )
    }
}