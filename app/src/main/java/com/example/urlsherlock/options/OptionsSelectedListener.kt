package com.example.urlsherlock.options

import com.example.urlsherlock.model.SearchOptions

interface OptionsSelectedListener {
    fun onOptionsSelected(options: SearchOptions)
}